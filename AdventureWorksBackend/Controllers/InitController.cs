﻿using AdventureWorksBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdventureWorksBackend.Controllers
{
    public class InitController : ApiController
    {

        // GET: api/Init
        [Route("api/Init")]
        [HttpGet]
        public Object Init()
        {
            Dictionary<String, List<StateProvinceViewModel>> StateProvinces
                = new Dictionary<string, List<StateProvinceViewModel>>();

            // Group StateProvinces by Country
            foreach (StateProvinceViewModel stateProvince in GetStateProvinces())
            {
                if (!StateProvinces.ContainsKey(stateProvince.CountryRegionCode))
                {
                    StateProvinces[stateProvince.CountryRegionCode] = new List<StateProvinceViewModel>();
                }
                StateProvinces[stateProvince.CountryRegionCode].Add(stateProvince);
            }

            return new
            {
                AddressTypes = GetAddressTypes(),
                CountryRegions = GetCountryRegions(),
                StateProvinces
            };
        }


        private IEnumerable<AddressTypeViewModel> GetAddressTypes()
        {

            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                List<AddressTypeViewModel> addressTypes = new List<AddressTypeViewModel>();
                using (SqlCommand cmd = new SqlCommand(
                    "SELECT AddressTypeID,Name FROM Person.AddressType",
                    connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        addressTypes.Add(new AddressTypeViewModel
                        {
                            AddressTypeID = (int)reader["AddressTypeID"],
                            Name = (String)reader["Name"]
                        });
                    }
                    reader.Close();
                }
                return addressTypes;
            }
        }


        private IEnumerable<StateProvinceViewModel> GetStateProvinces()
        {

            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                List<StateProvinceViewModel> stateProvinces = new List<StateProvinceViewModel>();
                using (SqlCommand cmd = new SqlCommand(
                    @"SELECT 
                        P.StateProvinceID, P.StateProvinceCode, P.Name, P.CountryRegionCode
                      FROM Person.StateProvince AS P",
                    connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        stateProvinces.Add(new StateProvinceViewModel
                        {
                            StateProvinceID = (int)reader["StateProvinceID"],
                            StateProvinceCode = (string)reader["StateProvinceCode"],
                            CountryRegionCode = (string)reader["CountryRegionCode"],
                            Name = (String)reader["Name"]
                        });
                    }
                    reader.Close();
                }
                return stateProvinces;
            }
        }

        private IEnumerable<CountryRegionViewModel> GetCountryRegions()
        {

            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                List<CountryRegionViewModel> countryRegions = new List<CountryRegionViewModel>();
                using (SqlCommand cmd = new SqlCommand(
                    "SELECT CountryRegionCode,Name FROM Person.CountryRegion",
                    connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        countryRegions.Add(new CountryRegionViewModel
                        {
                            CountryRegionCode = (string)reader["CountryRegionCode"],
                            Name = (String)reader["Name"]
                        });
                    }
                    reader.Close();
                }
                return countryRegions;
            }
        }
    }
}

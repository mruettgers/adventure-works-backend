﻿using AdventureWorksBackend.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Http;

namespace AdventureWorksBackend.Controllers
{
    public class CustomersController : ApiController
    {
        private const int itemsPerPage = 50;

        // GET: api/customers
        [Route("api/customers")]
        public Object GetAll(
            [FromUri] int page = 1,
            [FromUri] string orderBy = null,
            [FromUri] bool orderDesc = false
            )
        {
            return new
            {
                meta = new
                {
                    itemsPerPage,
                    page,
                    totalCount = GetCustomerCount()
                },
                items = GetCustomers(page, orderBy, orderDesc)
            };
        }


        // GET: api/customers/5
        [Route("api/customers/{customerId}")]
        public CustomerViewModel Get(int customerID)
        {
            return GetCustomer(customerID);
        }

        // GET: api/customers/5
        [Route("api/customers/{customerId}/orders")]
        public Object GetOrdersByCustomer(int customerId)
        {
            return new
            {
                meta = new { },
                items = GetOrders(customerId)
            };
        }

        private IEnumerable<CustomerViewModel> GetCustomers(int page, string orderBy, bool orderDesc)
        {
            string orderByField = null;
            switch (orderBy)
            {
                default:
                    orderByField = "C.CustomerID";
                    break;
            }


            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                List<CustomerViewModel> customers = new List<CustomerViewModel>();
                string query = GetCustomerBaseQuery() +
                    (orderByField != null ? " ORDER BY " + orderByField + (orderDesc ? " DESC" : "") : "") +
                    " OFFSET " + (((page > 0 ? page : 1) - 1) * itemsPerPage) + " ROWS FETCH NEXT " + itemsPerPage + " ROWS ONLY";

                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        customers.Add(new CustomerViewModel
                        {
                            BusinessEntityID = (int)reader["BusinessEntityID"],
                            CustomerID = (int)reader["CustomerID"],
                            FirstName = reader["FirstName"] as string,
                            LastName = reader["LastName"] as string
                        });
                    }
                    reader.Close();
                }


                foreach (CustomerViewModel customer in customers)
                {
                    customer.Addresses = GetAddresses(customer.BusinessEntityID);
                    customer.EmailAddresses = GetEmailAddresses(customer.BusinessEntityID);
                }
                return customers;
            }
        }


        private CustomerViewModel GetCustomer(int customerID)
        {
            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                List<CustomerViewModel> customers = new List<CustomerViewModel>();
                string query = GetCustomerBaseQuery() +
                    " AND C.CustomerID = " + customerID;

                using (SqlCommand cmd = new SqlCommand(query, connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        int businessEntityID = (int)reader["BusinessEntityID"];
                        return new CustomerViewModel
                        {
                            BusinessEntityID = businessEntityID,
                            CustomerID = customerID,
                            FirstName = reader["FirstName"] as string,
                            LastName = reader["LastName"] as string,
                            Addresses = GetAddresses(businessEntityID),
                            EmailAddresses = GetEmailAddresses(businessEntityID)
                        };
                    }
                    reader.Close();
                }
            }
            return null;
        }

        private int GetCustomerCount()
        {
            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GetCustomerBaseQuery(true), connection))
                {
                    return (int)cmd.ExecuteScalar();
                }
            }
        }

        private string GetCustomerBaseQuery(bool AsCount = false)
        {
            return "SELECT " +
                    (AsCount ? "COUNT(*) AS Total" : "C.CustomerID AS CustomerID, BE.BusinessEntityID, P.FirstName, P.LastName ") +
                    " FROM Person.BusinessEntity AS BE" +
                    " INNER JOIN Person.Person AS P ON P.BusinessEntityID = BE.BusinessEntityID" +
                    " INNER JOIN Sales.Customer AS C ON C.PersonID = BE.BusinessEntityID" +
                    " WHERE C.CustomerID >= 11000";
        }

        private List<AddressViewModel> GetAddresses(int businessEntityID)
        {
            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                List<AddressViewModel> addresses = new List<AddressViewModel>();
                using (SqlCommand cmd = new SqlCommand(@"
                        SELECT 
                            A.AddressID, A.AddressLine1, A.AddressLine2, A.City, A.PostalCode, BA.AddressTypeID, A.StateProvinceID 
                        FROM Person.BusinessEntityAddress AS BA
                        INNER JOIN Person.Address AS A ON BA.AddressID = A.AddressID 
                        WHERE BA.BusinessEntityID = " + businessEntityID,
                    connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        addresses.Add(new AddressViewModel
                        {
                            AddressID = (int) reader["AddressID"],
                            AddressLine1 = reader["AddressLine1"] as string,
                            AddressLine2 = reader["AddressLine2"] as string,
                            AddressTypeID = (int)reader["AddressTypeID"],
                            City = reader["City"] as string,
                            PostalCode = reader["PostalCode"] as string,
                            StateProvinceID = (int)reader["StateProvinceID"]
                        });
                    };
                    reader.Close();
                }
                return addresses;
            }
        }


        private List<EmailAddressViewModel> GetEmailAddresses(int businessEntityID)
        {
            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                List<EmailAddressViewModel> emailAddresses = new List<EmailAddressViewModel>();
                using (SqlCommand cmd = new SqlCommand(@"
                        SELECT EmailAddressID, EmailAddress FROM Person.EmailAddress AS EA
                        WHERE BusinessEntityID = " + businessEntityID,
                    connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        emailAddresses.Add(new EmailAddressViewModel
                        {
                            EmailAddressID = (int)reader["EmailAddressID"],
                            EmailAddress = reader["EmailAddress"] as string
                        });
                    };
                    reader.Close();
                }
                return emailAddresses;
            }
        }

        private List<OrderViewModel> GetOrders(int customerID)
        {
            using (SqlConnection connection = new SqlConnection(WebApiApplication.ConnectionString))
            {
                connection.Open();
                List<OrderViewModel> orders = new List<OrderViewModel>();
                using (SqlCommand cmd = new SqlCommand(@"
                        SELECT SalesOrderID, SalesOrderNumber, SalesOrderID, OrderDate, SubTotal FROM Sales.SalesOrderHeader
                        WHERE CustomerID = " + customerID,
                    connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        orders.Add(new OrderViewModel
                        {
                            SalesOrderID = (int)reader["SalesOrderID"],
                            OrderNumber = reader["SalesOrderNumber"] as string,
                            OrderDate = (DateTime)reader["OrderDate"],
                            Total = (decimal)reader["SubTotal"]
                        });
                    };
                    reader.Close();
                }
                return orders;
            }
        }
    }
}
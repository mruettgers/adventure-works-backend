﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdventureWorksBackend.ViewModels
{
    public class OrderViewModel
    {
        [Required]
        public int SalesOrderID { get; set; }

        [Required]
        [StringLength(25)]
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }

        public decimal Total { get; set; }

    }
}
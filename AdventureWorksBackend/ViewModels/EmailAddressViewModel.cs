﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdventureWorksBackend.ViewModels
{
    public class EmailAddressViewModel
    {

        public int EmailAddressID { get; set; }

        [StringLength(50)]
        public string EmailAddress { get; set; }
    }
}
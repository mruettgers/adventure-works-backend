﻿using AdventureWorksBackend.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdventureWorksBackend.ViewModels
{
    public class CustomerViewModel
    {

        public int CustomerID { get; set; }
        public int BusinessEntityID { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        public List<EmailAddressViewModel> EmailAddresses;

        public List<AddressViewModel> Addresses;
    }
}